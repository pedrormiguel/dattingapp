import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslationWidth } from '@angular/common';

@Component({
  selector: 'app-value', // Esto es el selector el nombre del la etiqueta para ser incluida dentro de nuestro archivos
  templateUrl: './value.component.html', //  El template donde esta el "HTML"
  styleUrls: ['./value.component.css'] // la hoja de estilos que aplicara
})
export class ValueComponent implements OnInit {

    values: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getValues();
  }

   getValues() {
     this.http.get('https://localhost:5001/api/values/index').subscribe(
      result => {
        this.values = result;
     }, error => {
       console.log(error);
     });
   }

}
