﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DatingApp.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")] 
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly DataContext _context;

        public ValuesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<string> Home()
        {
            return "hello";
        }

        // GET: /<controller>/
        [HttpGet("Index")]
        public IActionResult Index()
        {
            var values = _context.Values.ToList();

            return Ok(values);
        }

        [AllowAnonymous]
        [HttpGet("getvalue/{id}")]
       //[Route("search")]
        public IActionResult GetValue( int id ) 
        {
            var value = _context.Values.FirstOrDefault(x => x.Id == id);

            return Ok(value);
        }
    }
}
